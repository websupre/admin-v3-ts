import {defineStore} from 'pinia'
import {PersistedStateOptions} from "pinia-plugin-persistedstate";

export const themeStore = defineStore('themeStore', {
    state: () => ({
        themeType: 'BLUE_WHITE',//主题类型
    }),
    persist: {
        paths: ['themeType']
    } as PersistedStateOptions,
    actions: {
        /**
         * 设置主题类型
         * @param themeType
         */
        setThemeType(themeType: 'FACTORY' | 'BLUE' | 'BLUE_WHITE') {
            this.themeType = themeType;
            document.getElementsByTagName('body')[0].className = themeType;
        }
    }
})
