import {createPinia} from 'pinia'
//状态持久化
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
//注册持久化
const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);
export default pinia;
