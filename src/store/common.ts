import {defineStore} from "pinia";
import {commonStoreType, userInfo} from '@/types/common'

export const commonStore = defineStore('commonStore', {
    state: () => {
        return {
            i18nType: 'zh',
            userInfo: {},
            auth: {},
        } as commonStoreType
    },
    persist: true,
    actions: {
        /**
         * 设置语言类型
         * @param i18nType
         */
        setI18nType(i18nType: string) {
            this.i18nType = i18nType;
        },
        /**
         * 设置用户信息
         * @param info
         */
        setUserInfo(info: userInfo) {
            this.userInfo = info;
        },
        setAuth(auth: any) {
            this.auth = auth;
        }
    }
})
