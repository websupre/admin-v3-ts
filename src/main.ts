import {createApp} from "vue";
import App from "./App.vue";

const app = createApp(App);






//全局状态
import pinia from "@/store";

//iconfont
import "@/assets/iconfont/iconfont";
import "@/assets/iconfont/iconfont.css";

import * as Icons from '@ant-design/icons-vue'
// 全局使用图标，遍历引入
const icons: any = Icons
for (const i in icons) {
  app.component(i, icons[i])
}
// 公共样式
import "@/styles/index.scss";


//路由
import router from "./router";
//WINDICSS
import "virtual:windi.css";

//http
// @ts-ignore
import http from "@/libs/util.http";
//渲染异常的组件可以在这里全局事先申明一下

app
    .use(pinia)
    .use(router as any)
    .mount("#app");
