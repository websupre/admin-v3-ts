/**
 * 公用状态类型
 * @param i18nType string 语言类型 zh 中文、en 英文
 */
export interface commonStoreType {
    i18nType: string
    userInfo: userInfo
    auth:any
}

export interface userInfo {
    id: string,
    headPic: string,
    userName: string,
    deptIdstr: string,
    lastLoginTime: string,
    account: string,
}

/**
 * Dkb模块状态类型
 * @param basicId string 文库id
 */
export interface dkbStoreType {
    basicId: string
    basicName: string
    docId: string
}
/**
 * CMB模块状态类型
 * @param basicId string 文库id
 */
 export interface cbmStoreType {
    planDetail: {}
    jumpType: string
    workOrderId: string
    inspectionData: {
        id: string;
        name: string;
        code: string;
        eqId: string;
        eqName: string;
        materialId: string;
        materialName: string;
        materialCode: string;
        unitId: string;
        unit: string;
        ioType: number;
        available: number;
        remark?: any;
        seq: number;
        calType: number;
        formula?: any;
    }
}
/**
 * PPM模块状态类型
 * @param basicId Arrar 文库selectTree
 * 设置考核维度定义范围树中数据
 */
 export interface ppmStoreType {
    execObjectId:string,
    performanceExecId:string,
    developTree:selectTree[],
    developTreeId:string[],
    operationType:string,
    assessmentId:string,
    dataSummary:{},
    stepOneInfo:{},
    reviewView:{},
    detailId:string,
    activeNum:number,
}
/**
 * 树
 */
interface selectTree{
    id: string;
    parentId: string;
    parentNodeId?: any;
    parentName?: any;
    nodeId: string;
    nodeCode: string;
    nodeName: string;
    nodeDicDetailId?: any;
    nodeDicDetailCode?: any;
    nodeDicDetailName?: any;
    nodeTypeId: string;
    nodeTypeName: string;
    seq: number;
    status: number;
    statusName: string;
    isBold: number;
    children: any[];
}
/**
 * 公用icons类型
 * @param EXPORT 导出
 * @param REFRESH 刷新、重置
 * @param PLAY 开始播放、启用、启动
 * @param STOP 停止,暂停播放、停用、停止、暂停
 * @param ADD 新增、添加
 * @param DEL 删除、销毁
 * @param SAVE 保存
 * @param EXCEL excel
 * @param FILE 文件夹
 * @param PPT ppt
 * @param TEXT text
 * @param JS js
 * @param VUE vue
 * @param OTHERFILE 其他文件
 * @param WORD word
 * @param PDF pdf
 * @param SCREENFULL 全屏
 * @param SHARE 分享
 * @param DOWNLOAD 下载
 * @param COLLECT 收藏
 * @param COPY 复制链接
 * @param SORT 排序
 * @param ADDFILE 添加
 * @param CLOSE 关闭
 * @param EDIT 编辑
 * @param MOVE 移动
 * @param HISTORY 历史
 * @param PERSON 人
 * @param SCLOSE 叉叉
 * @param BACK 返回
 * @param FORWARD 前进
 * @param DOUBLEFORWARD 下一年年切换
 * @param DOUBLEBACK 上一年切换
 * @param PROCESS 流程
 * @param ARROW_DOUBLE_R 双右箭头
 * @param ARROW_DOUBLE_L 双左箭头
 */
export enum icons {
    'EXPORT' = 'icona-huaban1fuben23',
    'REFRESH' = 'icona-huaban1',
    'PLAY' = 'iconbofang-xian',
    'STOP' = 'iconzanting-xian',
    'ADD' = 'iconadd',
    'DEL' = 'icontrash1',
    'SAVE' = 'iconzhiguanshenhe',
    'EXCEL' = 'iconexcel',
    'FILE' = 'iconwenjianjia',
    'PPT' = 'iconPPT',
    'TEXT' = 'icontxt-1',
    'JS' = 'iconjs',
    'VUE' = 'iconVue',
    'OTHERFILE' = 'iconqitawenjian',
    'WORD' = 'iconword',//'iconDOCX',
    'PDF' = 'iconPDF',
    'SCREENFULL' = 'iconquanping',
    'SHARE' = 'iconfenxiang',
    'COPY' = 'iconhuixingzhen',
    'DOWNLOAD' = 'iconxiazai',
    'COLLECT' = 'iconshoucang',
    'SORT' = 'iconpaixu',
    'ADDFILE' = 'icontianjia1',
    'CLOSE' = 'icona-huaban1fuben18',
    'EDIT' = 'iconbianji',
    'MOVE' = 'iconfudu-xian',
    'HISTORY' = 'iconshijian-xian',
    'PERSON' = 'icona-huaban1fuben6',
    'SCLOSE' = 'iconguanbi-xian',
    'BACK' = 'icona-huaban1fuben44',
    'FORWARD' = 'icona-huaban1fuben45',
    'DOUBLEFORWARD' = 'iconarrow-double-right',
    'DOUBLEBACK' = 'iconimoo_icon_zuo',
    'PROCESS' = 'iconkaoqinjilu-xian',
    'ARROW_DOUBLE_R' = 'iconarrow-double-right',
    'ARROW_DOUBLE_L' = 'iconimoo_icon_zuo',
}

/**
 * 公用接口
 * @params TREE 树
 */
export enum commonUrls {
    'TREE' = '/api/services/app/Eip_03_Job/LevelTreeWithJobAuthGetFromCode',
}

/**
 * @description 树
 * @param levelCode string 具体类型参考工程模型=>数据字典=>层级类型
 * @param showPosition number 是否显示岗位 0 不显示、1 显示
 */
export interface TreeParams {
    levelCode: string
    showPosition: number
}
