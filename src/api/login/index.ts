import http from '@/libs/util.http'
import * as T from './types'
import Urls from './apis'

/**
 * login模块api
 * @author zc
 * @date 2023/06/15
 * @param {object} params 接口参数
 * @param {object} config 接口配置 比如：[loading]是否开启loading, axios的基础配置也可使用详情请看/libs/util.http
 * @example api.login({ account: 'xxx', password: 'xxxxx', }, {  })
 */
const api: T.IApi = {
    login(params, config) {
        return http.post(
            Urls.LOGIN_API,
            params,
            config,
        )
    },
    userInfo(params, config) {
        return http.post(
            Urls.USER_INFO,
            params,
            config,
        )
    }
}

export default api
