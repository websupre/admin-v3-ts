export interface Config {
    loading?: boolean
}

/**
 * @author zc
 * @date 2023/06/15
 * @description 登录接口
 * @param account string 账号
 * @param password string 密码
 * @param isDomain string 是否为域账户
 * @param sessionId string 用户后端验证对比,前端生成uuid
 * @param tenantCode string 租户code
 * @param validCode string 验证码
 */
export interface LoginParams {
    account: string
    password: string
    isDomain: boolean
    sessionId: string
    tenantCode: string
    validCode: string
}

export interface userInfo {

}


export interface IApi {
    login: (params: LoginParams, config?: Config,) => Promise<any>,
    userInfo: (params: userInfo, config?: Config,) => Promise<any>,
}
