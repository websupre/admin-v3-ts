/**
 * 接口urls统一管理，如果项目庞大可拆分模块管理
 * @author zc
 * @date 2023/06/15
 * @description 使用枚举对应，键名全大写加'_'间隔
 */

enum Urls {
    'LOGIN_API' = '/api/CISToken/Generator',
    'USER_INFO' = '/api/services/app/Eip_01_User/UserInfoForUser',
}

export default Urls
