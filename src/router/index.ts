import { createRouter, createWebHashHistory } from "vue-router";
import type { RouteRecordRaw } from "vue-router";
import Main from "@/layouts/main/index.vue";
// import Login from "@/pages/login/index.vue";
import { commonStore } from "@/store/common";

let routes: RouteRecordRaw[] = [
  {
    path: "/",
    redirect: {
      name: "Home",
    },
    component: Main,
    children: [
      {
        path: "Home",
        name: `Home`,
        meta: {
          title: "首页",
        },
        component: () => import("@/pages/Home/index.vue"),
      },
      {
        path: "About",
        name: `About`,
        meta: {
          title: "关于公司",
        },
        component: () => import("@/pages/AboutCompany/index.vue"),
      },
      {
        path: "PersonalCenter",
        name: `PersonalCenter`,
        meta: {
          title: "个人中心",
        },
        component: () => import("@/pages/PersonalCenter/index.vue"),
      },
      {
        path: "UserManagement",
        name: `UserManagement`,
        meta: {
          title: "用户管理",
        },
        component: () => import("@/pages/UserManagement/index.vue"),
      },
      {
        path: "NewsColumnManagement",
        name: `NewsColumnManagement`,
        meta: {
          title: "新闻栏目管理",
        },
        component: () => import("@/pages/NewsColumnManagement/index.vue"),
      },
      {
        path: "NewsInformationManagement",
        name: `NewsInformationManagement`,
        meta: {
          title: "新闻资讯管理",
        },
        component: () => import("@/pages/NewsInformationManagement/index.vue"),
      },
      {
        path: "MessageManagement",
        name: `MessageManagement`,
        meta: {
          title: "问题留言解答",
        },
        component: () => import("@/pages/MessageManagement/index.vue"),
      },
      {
        path: "RuleConfiguration",
        name: `RuleConfiguration`,
        meta: {
          title: "规则配置",
        },
        component: () => import("@/pages/RuleConfiguration/index.vue"),
      },
      {
        path: "TemplateManagement",
        name: `TemplateManagement`,
        meta: {
          title: "模版管理",
        },
        component: () => import("@/pages/TemplateManagement/index.vue"),
      },
      {
        path: "ComplaintAcceptance",
        name: `ComplaintAcceptance`,
        meta: {
          title: "投诉受理",
        },
        component: () => import("@/pages/ComplaintAcceptance/index.vue"),
      },
      {
        path: "ComplaintHandling",
        name: `ComplaintHandling`,
        meta: {
          title: "投诉办理",
        },
        component: () => import("@/pages/ComplaintHandling/index.vue"),
      },
      {
        path: "ComplaintWarning",
        name: `ComplaintWarning`,
        meta: {
          title: "投诉预警",
        },
        component: () => import("@/pages/ComplaintWarning/index.vue"),
      },
      {
        path: "StatisticalAnalysis",
        name: `StatisticalAnalysis`,
        meta: {
          title: "统计分析",
        },
        component: () => import("@/pages/StatisticalAnalysis/index.vue"),
      },
    ],
  },
  // {
  //   path: "/login",
  //   name: "login",
  //   component: Login,
  // },
  
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

router.beforeEach(async (to, from, next) => {
  const commonStores = commonStore();
  const { meta, name } = to;
  meta.pageAuth = commonStores.auth[name as string] || [];
  next();
});
export default router;
